﻿namespace Shifrovanie_RSA
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.Save_shifr = new System.Windows.Forms.Button();
            this.Shifr_rasshifr = new System.Windows.Forms.Button();
            this.Save_text = new System.Windows.Forms.Button();
            this.Load_text = new System.Windows.Forms.Button();
            this.Key = new System.Windows.Forms.Button();
            this.Key_text = new System.Windows.Forms.TextBox();
            this.Text_load = new System.Windows.Forms.TextBox();
            this.Shifr_Text = new System.Windows.Forms.TextBox();
            this.Open_file = new System.Windows.Forms.OpenFileDialog();
            this.Save_file = new System.Windows.Forms.SaveFileDialog();
            this.Open_key = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(631, 375);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(101, 17);
            this.radioButton2.TabIndex = 27;
            this.radioButton2.Text = "Расшифровать";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.radioButton1.Location = new System.Drawing.Point(463, 375);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(95, 17);
            this.radioButton1.TabIndex = 26;
            this.radioButton1.Text = "Зашифровать";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // Save_shifr
            // 
            this.Save_shifr.Location = new System.Drawing.Point(587, 312);
            this.Save_shifr.Name = "Save_shifr";
            this.Save_shifr.Size = new System.Drawing.Size(176, 23);
            this.Save_shifr.TabIndex = 25;
            this.Save_shifr.Text = "Сохранить шифрованный текст";
            this.Save_shifr.UseVisualStyleBackColor = true;
            this.Save_shifr.Click += new System.EventHandler(this.Save_shifr_Click);
            // 
            // Shifr_rasshifr
            // 
            this.Shifr_rasshifr.Location = new System.Drawing.Point(414, 312);
            this.Shifr_rasshifr.Name = "Shifr_rasshifr";
            this.Shifr_rasshifr.Size = new System.Drawing.Size(167, 23);
            this.Shifr_rasshifr.TabIndex = 24;
            this.Shifr_rasshifr.Text = "Зашифровать/Расшифровать";
            this.Shifr_rasshifr.UseVisualStyleBackColor = true;
            this.Shifr_rasshifr.Click += new System.EventHandler(this.Shifr_rasshifr_Click);
            // 
            // Save_text
            // 
            this.Save_text.Location = new System.Drawing.Point(195, 312);
            this.Save_text.Name = "Save_text";
            this.Save_text.Size = new System.Drawing.Size(102, 23);
            this.Save_text.TabIndex = 23;
            this.Save_text.Text = "Сохранить текст";
            this.Save_text.UseVisualStyleBackColor = true;
            this.Save_text.Click += new System.EventHandler(this.Save_text_Click);
            // 
            // Load_text
            // 
            this.Load_text.Location = new System.Drawing.Point(47, 312);
            this.Load_text.Name = "Load_text";
            this.Load_text.Size = new System.Drawing.Size(104, 23);
            this.Load_text.TabIndex = 22;
            this.Load_text.Text = "Загрузить текст";
            this.Load_text.UseVisualStyleBackColor = true;
            this.Load_text.Click += new System.EventHandler(this.Load_text_Click);
            // 
            // Key
            // 
            this.Key.Location = new System.Drawing.Point(176, 374);
            this.Key.Name = "Key";
            this.Key.Size = new System.Drawing.Size(92, 51);
            this.Key.TabIndex = 21;
            this.Key.Text = "Сгенерировать";
            this.Key.UseVisualStyleBackColor = true;
            this.Key.Click += new System.EventHandler(this.Key_Click);
            // 
            // Key_text
            // 
            this.Key_text.Location = new System.Drawing.Point(61, 402);
            this.Key_text.Name = "Key_text";
            this.Key_text.Size = new System.Drawing.Size(100, 20);
            this.Key_text.TabIndex = 20;
            // 
            // Text_load
            // 
            this.Text_load.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.Text_load.Location = new System.Drawing.Point(12, 27);
            this.Text_load.Margin = new System.Windows.Forms.Padding(2);
            this.Text_load.Multiline = true;
            this.Text_load.Name = "Text_load";
            this.Text_load.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Text_load.Size = new System.Drawing.Size(372, 271);
            this.Text_load.TabIndex = 18;
            // 
            // Shifr_Text
            // 
            this.Shifr_Text.Location = new System.Drawing.Point(403, 27);
            this.Shifr_Text.Multiline = true;
            this.Shifr_Text.Name = "Shifr_Text";
            this.Shifr_Text.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.Shifr_Text.Size = new System.Drawing.Size(386, 271);
            this.Shifr_Text.TabIndex = 19;
            // 
            // Open_file
            // 
            this.Open_file.FileName = "openFileDialog1";
            // 
            // Open_key
            // 
            this.Open_key.Location = new System.Drawing.Point(61, 374);
            this.Open_key.Name = "Open_key";
            this.Open_key.Size = new System.Drawing.Size(100, 20);
            this.Open_key.TabIndex = 28;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(64, 358);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Открытый ключ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(67, 429);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Закрытый ключ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Open_key);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.Save_shifr);
            this.Controls.Add(this.Shifr_rasshifr);
            this.Controls.Add(this.Save_text);
            this.Controls.Add(this.Load_text);
            this.Controls.Add(this.Key);
            this.Controls.Add(this.Key_text);
            this.Controls.Add(this.Text_load);
            this.Controls.Add(this.Shifr_Text);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Button Save_shifr;
        private System.Windows.Forms.Button Shifr_rasshifr;
        private System.Windows.Forms.Button Save_text;
        private System.Windows.Forms.Button Load_text;
        private System.Windows.Forms.Button Key;
        private System.Windows.Forms.TextBox Key_text;
        private System.Windows.Forms.TextBox Text_load;
        private System.Windows.Forms.TextBox Shifr_Text;
        private System.Windows.Forms.OpenFileDialog Open_file;
        private System.Windows.Forms.SaveFileDialog Save_file;
        private System.Windows.Forms.TextBox Open_key;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

