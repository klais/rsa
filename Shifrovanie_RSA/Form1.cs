﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Numerics;

namespace Shifrovanie_RSA
{

    public partial class Form1 : Form
    {
        int open_key, close_key, d;
        byte[] len;
        int[] shifr;

        Random rand  = new Random();
        public Form1()
        {
            InitializeComponent();

            Save_file.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";
            Open_file.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";
        }

        private long Evklid(long first, long second)
        {
            while (first != 0 && second != 0)
            {
                if (first >= second) first = first % second;
                else second = second % first;
            }
            return first + second;
        }
        int Big_Evklid(int first, int second, out int x_first, out int y_first)
        {
            if (first == 0)
            {
                x_first = 0; y_first = 1;
                return second;
            }
            int x_second, y_second;
            int d = Big_Evklid(second % first, first, out x_second, out y_second);
            x_first = y_second - (second / first) * x_second;
            y_first = x_second;
            return d;
        }

        

        private bool Fill(long x)
        {
            if (x == 0)
                return false;
            if (x == 2)
                return true;

            for (int i = 0; i < 100; i++)
            {
                long a = rand.Next((int)x);
                if (a <= 2)
                    a = a + 2;
                if (BigInteger.ModPow(a, x - 1, x) != 1)
                {
                    return false;
                }
            }
            return true;
        }

       

        private void RSA(out int e, out int n, out int d)
        {
            int p, q, f, l;
            bool c;
            do
            {
                p = rand.Next(32000);
                c = Fill(p);
            }
            while (!c);
            q = p;
            do
            {
                q++;
                c = Fill(q);
            }
            while (!c);
            n = p * q;
            f = (p - 1) * (q - 1);
            do
            {
                e = rand.Next(6000);
                Big_Evklid(e, f, out l, out d);
            }
            while ((Evklid(f, e) != 1) || (l < 0));
            d = l;

        }

        

        private void Save_text_Click(object sender, EventArgs e)
        {

            if (Save_file.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = Save_file.FileName;
            System.IO.File.WriteAllText(filename, Text_load.Text);
        }

        private void Load_text_Click(object sender, EventArgs e)
        {
            if (Open_file.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = Open_file.FileName;
            Text_load.Text = System.IO.File.ReadAllText(filename);
        }
        private void Save_shifr_Click(object sender, EventArgs e)
        {
            if (Save_file.ShowDialog() == DialogResult.Cancel)
                return;
            string filename = Save_file.FileName;
            System.IO.File.WriteAllText(filename, Shifr_Text.Text);
        }

        private void Key_Click(object sender, EventArgs e)
        {
            RSA(out open_key, out close_key, out d);
            Open_key.Text = Convert.ToString(open_key);
            Key_text.Text = Convert.ToString(close_key);
        }
        private void Shifr_rasshifr_Click(object sender, EventArgs e)
        {
            if (radioButton1.Checked)
            {

                string S = Text_load.Text;
                len = Encoding.GetEncoding(1251).GetBytes(S);

                shifr = new int[len.Length];


                open_key = Convert.ToInt32(Open_key.Text);
                close_key = Convert.ToInt32(Key_text.Text);

                for (int i = 0; i < len.Length; i++)
                {
                    shifr[i] = (int)BigInteger.ModPow(len[i], open_key, close_key);
                    Shifr_Text.Text += Convert.ToString(shifr[i]);

                }
            }
            if (radioButton2.Checked)
            {
                close_key = Convert.ToInt32(Key_text.Text);
                for (int i = 0; i < len.Length; i++)
                {
                    len[i] = (byte)BigInteger.ModPow(shifr[i], d, close_key);
                }

                string decod = Encoding.GetEncoding(1251).GetString(len);
                Text_load.Text = decod;
            }
        }

       

       

    }
}
